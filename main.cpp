#include <PropWare/hmi/output/printer.h>
#include <PropWare/serial/i2c/i2cmaster.h>
using PropWare::I2CMaster;
#include "crc.h"

I2CMaster i2c(PropWare::Pin::P19, PropWare::Pin::P18, 100000);

const uint8_t SLAVE_ADDR = (0x08 << 1);

// register map
#define SYS_STAT 0x00
#define CELLBAL1 0x01
#define CELLBAL2 0x02
#define CELLBAL3 0x03
#define SYS_CTRL1 0x04
#define SYS_CTRL2 0x05
#define PROTECT1 0x06
#define PROTECT2 0x07
#define PROTECT3 0x08
#define OV_TRIP 0x09
#define UV_TRIP 0x0A
#define CC_CFG 0x0B

#define VC1_HI 0x0C
#define VC1_LO 0x0D
#define VC2_HI 0x0E
#define VC2_LO 0x0F
#define VC3_HI 0x10
#define VC3_LO 0x11
#define VC4_HI 0x12
#define VC4_LO 0x13
#define VC5_HI 0x14
#define VC5_LO 0x15
#define VC6_HI 0x16
#define VC6_LO 0x17
#define VC7_HI 0x18
#define VC7_LO 0x19
#define VC8_HI 0x1A
#define VC8_LO 0x1B
#define VC9_HI 0x1C
#define VC9_LO 0x1D
#define VC10_HI 0x1E
#define VC10_LO 0x1F
#define VC11_HI 0x20
#define VC11_LO 0x21
#define VC12_HI 0x22
#define VC12_LO 0x23
#define VC13_HI 0x24
#define VC13_LO 0x25
#define VC14_HI 0x26
#define VC14_LO 0x27
#define VC15_HI 0x28
#define VC15_LO 0x29

#define BAT_HI 0x2A
#define BAT_LO 0x2B

#define TS1_HI 0x2C
#define TS1_LO 0x2D
#define TS2_HI 0x2E
#define TS2_LO 0x2F
#define TS3_HI 0x30
#define TS3_LO 0x31

#define CC_HI 0x32
#define CC_LO 0x33

#define ADCGAIN1 0x50
#define ADCOFFSET 0x51
#define ADCGAIN2 0x59

union Reg_SYS_STAT {
    struct {
        uint8_t OCD : 1;
        uint8_t SCD : 1;
        uint8_t OV : 1;
        uint8_t UV : 1;
        uint8_t OVRD_ALERT : 1;
        uint8_t DEVICE_XREADY : 1;
        uint8_t WAKE : 1;
        uint8_t CC_READY : 1;
    } bits;
    uint8_t raw;
};

struct bq769x0 {
    bq769x0(const uint8_t slave_addr, const int num_cells, const int ov_thresh, const int uv_thresh, const int bal_thresh);
    void writeReg(uint8_t addr, uint8_t val);
    uint8_t readReg(uint8_t addr, bool& valid);
    bool readVoltages();
    bool update();

    uint8_t slave_addr;
    int num_cells;
    bool error;

    // uV
    int adc_gain;
    // mV
    int adc_offset;
    // mV
    int cell_volt[15] = { 0 };
    // mV
    int string_volt = 0;
    // mV
    int ov_thresh = 0;
    // mV
    int uv_thresh = 0;
    // mV - when to enable balance circuit
    int bal_thresh = 0;
};

bq769x0::bq769x0(const uint8_t slave_addr, const int num_cells, const int ov_thresh, const int uv_thresh, const int bal_thresh)
    : slave_addr(slave_addr), num_cells(num_cells), ov_thresh(ov_thresh), uv_thresh(uv_thresh), bal_thresh(bal_thresh) {

    // datasheet says this reg should be written to 0x19
    writeReg(CC_CFG, 0x19);
    // read back to check if device is talking
    bool valid  = true;
    uint8_t reg = readReg(CC_CFG, valid);
    if(not valid or reg != 0x19) {
        error = true;
        return;
    } else {
        // continue init

        // enable ADC, use internal thermistors
        writeReg(SYS_CTRL1, 1 << 4);

        // get ADC offset and gain
        adc_offset = (int)readReg(ADCOFFSET, valid); // convert from 2's complement
        adc_gain   = 365 + (((readReg(ADCGAIN1, valid) & 12) << 1) |
                             ((readReg(ADCGAIN2, valid) & 224) >> 5)); // uV/LSB
        if(not valid) {
            error = true;
            return;
        }

        // set voltage trip thresholds
        uint8_t uv_trip = ((((long)uv_thresh - adc_offset) * 1000 / adc_gain) >> 4) & 0xFF;
        uint8_t ov_trip = ((((long)ov_thresh - adc_offset) * 1000 / adc_gain) >> 4) & 0xFF;
        pwOut << "uv_trip " << uv_trip << '\n';
        pwOut << "ov_trip " << ov_trip << '\n';
        writeReg(UV_TRIP, uv_trip);
        writeReg(OV_TRIP, ov_trip);

        // clear faults
        writeReg(SYS_STAT, 0xFF);
    }
}

void bq769x0::writeReg(uint8_t addr, uint8_t val) {
    crc_t crc = crc_init();
    crc       = crc_update(crc, &slave_addr, 1);
    crc       = crc_update(crc, &addr, 1);
    crc       = crc_update(crc, &val, 1);
    crc       = crc_finalize(crc);

    i2c.start();
    i2c.send_byte(slave_addr);
    i2c.send_byte(addr);
    i2c.send_byte(val);
    i2c.send_byte(crc);
    i2c.stop();
}

uint8_t bq769x0::readReg(uint8_t addr, bool& valid) {
    i2c.start();
    i2c.send_byte(slave_addr);
    i2c.send_byte(addr);
    i2c.stop();
    i2c.start();
    i2c.send_byte(slave_addr | 0x1);
    uint8_t val    = i2c.read_byte(true);
    crc_t crc_read = i2c.read_byte(false);
    i2c.stop();

    crc_t crc               = crc_init();
    uint8_t slave_addr_read = slave_addr | 0x1;
    crc                     = crc_update(crc, &slave_addr_read, 1);
    crc                     = crc_update(crc, &val, 1);
    crc                     = crc_finalize(crc);
    if(crc != crc_read)
        valid = false;
    return val;
}

bool bq769x0::readVoltages() {
    bool valid = true;

    // read battery pack voltage
    int val     = (readReg(BAT_HI, valid) << 8) | readReg(BAT_LO, valid);
    string_volt = 4.0 * adc_gain * val / 1000.0 + 4 * adc_offset;
    for(int c = 0; c < num_cells; c++) {
        val          = ((readReg(VC1_HI + 2 * c, valid) & 0x3F) << 8) | readReg(VC1_LO + 2 * c, valid);
        cell_volt[c] = adc_gain * val / 1000.0 + adc_offset;
    }
    return valid;
}

bool bq769x0::update() {
    bool valid = true;
    Reg_SYS_STAT stat;
    stat.raw = readReg(SYS_STAT, valid);
    if(not valid) {
        return false;
    }

    if(not readVoltages())
        return false;

    if(stat.bits.OCD)
        pwOut << "OCD ";
    if(stat.bits.SCD)
        pwOut << "SCD ";
    if(stat.bits.OV)
        pwOut << "OV ";
    if(stat.bits.UV)
        pwOut << "UV ";
    if(stat.bits.OVRD_ALERT)
        pwOut << "OVRD_ALERT ";
    if(stat.bits.DEVICE_XREADY)
        pwOut << "DEVICE_XREADY ";
    if(stat.bits.CC_READY)
        pwOut << "CC_READY ";
    pwOut << '\n';

    uint8_t balance0_4   = 0;
    uint8_t balance5_9   = 0;
    uint8_t balance10_14 = 0;
    for(int c = 0; c < 5; c++) {
        if(cell_volt[c] > bal_thresh) {
            balance0_4 |= 1 << c;
            pwOut << cell_volt[c] << "* ";
        } else
            pwOut << cell_volt[c] << "  ";
    }
    writeReg(CELLBAL1, balance0_4);
    if(num_cells > 5) {
        for(int c = 5; c < 10; c++) {
            if(cell_volt[c] > bal_thresh) {
                balance5_9 |= 1 << (c-5);
                pwOut << cell_volt[c] << "* ";
            } else
                pwOut << cell_volt[c] << "  ";
        }
        writeReg(CELLBAL1, balance5_9);
    }
    if(num_cells > 10) {
        for(int c = 10; c < 15; c++) {
            if(cell_volt[c] > bal_thresh) {
                balance10_14 |= 1 << (c-10);
                pwOut << cell_volt[c] << "* ";
            } else
                pwOut << cell_volt[c] << "  ";
        }
        writeReg(CELLBAL1, balance10_14);
    }
    pwOut << '\n'
          << balance0_4 << ' ' << balance5_9 << ' ' << balance10_14;
}

int main() {
    bq769x0 bms(0x08 << 1, 10, 2800, 2400, 2750);
    if(bms.error)
        pwOut << "init fail\n";
    else
        pwOut << "init good\n";

    pwOut << "gain " << bms.adc_gain << '\n';
    pwOut << "offset " << bms.adc_offset << '\n';

    //bq769x0 bms(bq76930, 0x08);
    //PropWare::I2C i2c(PropWare::Pin::P18, PropWare::Pin::P19, 100000);
    //uint8_t array[2];
    while(true) {
        //bms.updateVoltages();
        bms.update();
        int sum_cells = 0;
        for(uint8_t c = 0; c < bms.num_cells; c++)
            sum_cells += bms.cell_volt[c];
        pwOut << '\n';
        pwOut << bms.string_volt << ' ' << sum_cells << '\n';
        waitcnt(CLKFREQ / 4 + CNT);
    }

    return 0;
}
