#!/bin/bash

pushd .

cd tools/pycrc
python pycrc.py --width 8 --poly 0x07 \
    --reflect-in False --xor-in 0x00 \
    --reflect-out False --xor-out 0x00 \
    --algorithm table-driven \
    --generate h -o ../../crc.h
python pycrc.py --width 8 --poly 0x07 \
    --reflect-in False --xor-in 0x00 \
    --reflect-out False --xor-out 0x00 \
    --algorithm table-driven \
    --generate c -o ../../crc.c

popd
